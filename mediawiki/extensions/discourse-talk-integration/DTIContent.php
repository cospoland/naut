<?php
namespace MediaWiki\Extension\DTI;
use ParserOptions;
use ParserOutput;
use TextContent;
use Title;
/**
 * Content wrapper for the markdown format
 *
 * @license GPL-2.0+
 * @author Pyy
 */
class DTIContent extends TextContent {
	public function __construct( $text ) {
		parent::__construct( $text, 'DiscourseTalk' );
	}
	public function getParserOutput(
		Title $title,
		$revId = null,
		ParserOptions $options = null,
		$generateHtml = true
	) {
                $html = "<div id='discourse-comments'></div>

<script type='text/javascript'>
  DiscourseEmbed = { discourseUrl: 'https://forums.subnautica.cos.ovh/',
                     discourseEmbedUrl: 'https://subnautica.cos.ovh/w/" . $title->getFullText() . "' };

  (function() {
    var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true;
    d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js';
    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d);
  })();
</script>";
		$pout = new ParserOutput( $html );
		return $pout;
	}
}
