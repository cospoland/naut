<?php
/**
 * @since 1.0.0
 *
 * @file
 *
 * @licence GNU GPL
 * @author Pyy
 */

$magicWords = array();

/** English
 * @author Pyy
 */
$magicWords['en'] = array(
   'infobox' => array( 0, 'infobox' ),
);

/** Polish
 * @author Pyy
 */
$magicWords['pl'] = array(
   'infobox' => array( 0, 'infobox' ),
);

