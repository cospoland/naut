<?php
namespace MediaWiki\Extension\NautInfobox;
use OutputPage;
use Skin;
use Parser;
/**
 * Hook handlers for naut infobox
 *
 * @license GPL-2.0+
 * @author Pyy
 */
class NautHooks {
	public static function onParserSetup( &$parser ) {
		$parser->setFunctionHook( 'infobox', 'MediaWiki\\Extension\\NautInfobox\\NautHooks::render' );
	}
	public static function render( $parser ) {
		$options = NautHooks::extractOptions(array_slice(func_get_args(), 1));
		$gen_label = false;
		$label = "";
		$g_buff = "";
		$g_buff_h = "";
		$gen_group = false;
		$g_classes = "";
		$output = <<<EOH
<aside class="portable-infobox pi-background pi-theme-wikia pi-layout-default">
EOH;
		foreach($options as $key => $value) {
			if (empty($value)) continue;
			$buffer = "";
			switch(preg_replace('/[0-9]+/', '', $key)) {
				case "tytuł":
					$gen_label = false;
					$buffer = <<<EOH
<h2 class="pi-item pi-item-spacing pi-title"><center>$value</center></h2>
EOH;
					break;
                                case "obraz":
                                        $gen_label = false;
                                        $fvalueo = wfFindFile( $value );
					if(!$fvalueo) {
                                                        $buffer = $buffer . <<<EOH
<figure class="pi-item pi-image">
<a href="https://subnautica.cos.ovh/w/Special:Upload" class="image image-thumbnail" title="$value">
Brakujący plik: $value
</a>
</figure>
EOH;
					} else {
						$fvalue = $fvalueo->getFullUrl();
                	                        $buffer = <<<EOH
<figure class="pi-item pi-image">
<a href="$fvalue" class="image image-thumbnail" title="">
<img src="$fvalue" class="pi-image-thumbnail" alt="" width="128" height="128" data-image-key="$value" data-image-name="$value">
</a>
</figure>
EOH;
					}
                                        break;
                                case "grupa_obrazów":
					$images = explode(',', $value);
                                        $gen_label = false;
					foreach($images as $ivalue) {
						$fvalueu = wfFindFile( $ivalue );
						if($fvalueu == false) {
							$buffer = $buffer . <<<EOH
<figure class="pi-item pi-image">
<a href="https://subnautica.cos.ovh/w/Special:Upload" class="image image-thumbnail" title="$ivalue">
Brakujący plik: $ivalue
</a>
</figure>
EOH;
						} else {
							$fvalue = $fvalueu->getFullUrl();
        		                                $buffer = $buffer . <<<EOH
<figure class="pi-item pi-image">
<a href="$fvalue" class="image image-thumbnail" title="$ivalue">
<img src="$fvalue" class="pi-image-thumbnail" alt="" width="128" height="128" data-image-key="$ivalue" data-image-name="$ivalue">
</a>
</figure>
EOH;
						}
					}
					break;
                                case "label":
					$gen_label = true;
					$label = $value;
					break;
                                case "text":
					if($gen_label) {
						$value = $parser->recursiveTagParseFully("\n" . $value);
	                                        $buffer = <<<EOH
<div class="pi-item pi-data pi-item-spacing pi-border-color">
<h3 class="pi-data-label pi-secondary-font">$label</h3>
<div class="pi-data-value pi-font">$value</div>
</div>
EOH;
						$gen_label = false;
					}
                                        break;
				case "grupa":
					$gen_label = false;
					if($value == "koniec") {
						$gen_group = false;
                                                if(!empty($g_buff))
							$buffer = "<section class=\"pi-item pi-group pi-border-color pi-collapse $g_classes\">$g_buff_h" . $g_buff .  "</section>";
					}
					else {
						$g_classes = $value;
						$gen_group = true;
						$g_buff = "";
					}
					break;
				case "nagłówek_grupy":
					$gen_label = false;
					$g_buff_h = "<h2 class=\"pi-item pi-header pi-secondary-font pi-item-spacing pi-secondary-background\">$value</h2>";
					break;
			}
			if($gen_group) $g_buff = $g_buff . $buffer;
			else $output = $output . $buffer;
		}
		return array( $output . "</aside>", 'noparse' => true, 'isHTML' => true );
	}
	public static function extractOptions( array $options ) {
		$results = array();
		$indexing = array();
		foreach ( $options as $option ) {
			$pair = explode( '=', $option, 2 );
			if ( count( $pair ) === 2 ) {
				$name = trim( $pair[0] );
				$value = trim( $pair[1] );
                                $indexing[$name]++;
				$results[$name . $indexing[$name]] = $value;
			}
			if ( count( $pair ) === 1 ) {
				$name = trim( $pair[0] );
				$results[$name] = true;
			}
		}
		return $results;
	}
	public static function BeforePageDisplay( OutputPage &$out, Skin &$skin ) {
		$out->addModules( 'ext.nautinfobox' );
		return true;
	}
}
