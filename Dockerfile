FROM hhvm/hhvm-proxygen
RUN apt update \
  && apt install -y imagemagick \ 
  && apt clean \
  && rm -rf /var/cache/apt/lists
ADD mediawiki /var/www/public
COPY site.ini /etc/hhvm/site.ini
