<?php
/**
 * Skin file for skin Boneshark.
 *
 * @file
 * @ingroup Skins
 */

if( !defined( 'MEDIAWIKI' ) ) {
	die( -1 );
}

/**
 * SkinTemplate class for Boneshark
 * @ingroup Skins
 */
class SkinBoneshark extends SkinTemplate {
	var $skinname = 'Boneshark', $stylename = 'Boneshark',
		$template = 'BonesharkTemplate', $useHeadElement = true;

	/**
	 * Add JavaScript via ResourceLoader
	 * Uncomment this function if your skin has a JS file or files
	 * Otherwise you won't need this function and you can safely delete it
	 *
	 * @param OutputPage $out
	 */
	public function initPage( OutputPage $out ) {
		parent::initPage( $out );
 		//$out->addModuleScripts( 'skins.boneshark.js' );
	}

	/**
	 * Add CSS via ResourceLoader
	 *
	 * @param $out OutputPage
	 */

	function setupSkinUserCss( OutputPage $out ) {
		parent::setupSkinUserCss( $out );

		$out->addMeta( 'charset', 'utf-8' );
                $styles = [ 'mediawiki.skinning.interface', 'skins.boneshark' ];
                Hooks::run( 'SkinBonesharkStyleModules', [ $this, &$styles ] );
		$out->addModuleStyles( $styles );

	}
 
}

/**
 * BaseTemplate class for Boneshark skin
 * @ingroup Skins
 */
class BonesharkTemplate extends BaseTemplate {
 	/**
 	 * Check if user is logged in.
 	 */
 	public function isUserLoggedIn() {
		if ( !is_null($this->data['username']) ) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Outputs the entire contents of the page
	 */
	public function execute() {
	$this->data['content_navigation']['views']['edit']['id'] = 'ca-edit';
        $this->data['nav_urls']['mainpage']['text'] = "Strona główna";
        $this->data['nav_urls']['upload']['text'] = "Wyślij plik";
        $this->data['nav_urls']['specialpages']['text'] = "Strony specjalne";
        $this->data['nav_urls']['whatlinkshere']['text'] = "Linkujące";
        $this->data['nav_urls']['recentchanges'] = $this->data['sidebar']['navigation'][1];
        $this->data['nav_urls']['recentchangeslinked']['text'] = "Zmiany w linkujących";
	$this->html( 'headelement' ); ?>
	<header>
		<a href="/w" id="site-link">
			<img alt="<?php $this->text( 'sitename' )?>" src="<?php echo $GLOBALS['wgLogoHD'] ?>" id="site-logo">
		</a>
		<nav>
			<ul><li class="caption">Nawigacja</li><?php
				foreach ( $this->data['nav_urls'] as $nav_urls) {
                                	if ( !is_null( $nav_urls ) && !empty($nav_urls['href']) ){
                                        	echo <<<HTML
                                                	<li><a href="{$nav_urls['href']}"><i class="fa {$nav_urls['icon_class']}"></i>{$nav_urls['text']}</a></li>
HTML;
                                        }
                                }?><li class="caption">Personalne</li><?php
				foreach ( $this->data['personal_urls'] as $personal_urls) {
                                        if ( !is_null( $personal_urls ) && !empty($personal_urls['href']) ) {
                                                echo <<<HTML
                                                        <li><a href="{$personal_urls['href']}">{$personal_urls['text']}</a></li>
HTML;
                                        }
                                }?>
			</ul>
		</nav>
		<?php echo $GLOBALS['wgNaut7Sidebar']?>
	</header>
        <?php if ( $this->html( 'sitenotice' ) ) {?>
	        <section id="sitenotice"><?php echo $this->html('sitenotice');?></section>
        <?php } ?>
	<?php if ($this->data['isarticle']){?>
	<section id="toolbar"><ul><?php
                foreach ( $this->data['content_navigation'] as $key => $value ) {
			foreach ( $value as $views ){
				if ( !is_null($views) ){?>
					<li class="<?php echo $views['class'];?>" title="<?php echo $views['text'];?>" id="<?php echo $views['id'];?>">
						<a href="<?php echo $views['href'];?>"><i class="fa <?php echo $views['icon_class']?>"></i><?php echo $views['text'];?></a>
					</li>
<?php				}
			}
		}?></ul>
	</section>
	<?php }?>
	<article id="content">
		<h1 id="firstHeading" class="firstHeading"><?php $this->html( 'title' ) ?></h1>
		<div id="mw-content-text">
			<div id="content">
				<?php $this->html( 'bodytext' ); ?>
			</div>
		</div>
	</article>
	<footer>
		<?php $this->printTrail(); ?>
	</footer>
</body>
</html><?php
	}
}
?>
