<?php
namespace MediaWiki\Extension\DTI;
use Title;
use OutputPage;
use ParserOutput;
/**
 * Hook handlers for markdown support
 *
 * @license GPL-2.0+
 * @author Pyy
 */
class DTIHooks {
	public static function onContentHandlerDefaultModelFor( Title $title, &$model ) {
		if ( $title->isTalkPage() ) {
			$model = 'DiscourseTalk';
			return false;
		}
		return true;
	}
	public static function addDiscourseScript($title) {?>
<div id='discourse-comments'></div>

<script type='text/javascript'>
  DiscourseEmbed = { discourseUrl: 'https://forums.subnautica.cos.ovh/',
                     discourseEmbedUrl: 'https://subnautica.cos.ovh/w/<?php echo $title->getPrefixedURL();?>' };

  (function() {
    var d = document.createElement('script'); d.type = 'text/javascript'; d.async = true;
    d.src = DiscourseEmbed.discourseUrl + 'javascripts/embed.js';
    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(d);
  })();
</script>
<?php
	}
}
