<?php
namespace MediaWiki\Extension\DTI;
use TextContentHandler;
use Title;
/**
 * Content handler for the markdown format
 *
 * @license GPL-2.0+
 * @author Pyy
 */
class DTIHandler extends TextContentHandler {
	public function __construct() {
		parent::__construct( 'DiscourseTalk', [ 'text/html' ] );
	}
	protected function getContentClass() {
		return DTIContent::class;
	}
	public function supportsSections() {
		return false;
	}
	public function supportsCategories() {
		return false;
	}
	public function supportsRedirects() {
		return false;
	}
}
