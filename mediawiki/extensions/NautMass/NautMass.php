<?php
namespace MediaWiki\Extension\NautMass;
use SpecialPage;
/**
 * Naut MASS authentication provider
 *
 * @license GPL-2.0+
 * @author Pyy
 */
class NautMass extends SpecialPage {
	function __construct() {
		parent::__construct( 'NautMass', 'massaccess' );
	}

	function execute( $par ) {
		$request = $this->getRequest();
		if ( !$this->userCanExecute( $this->getUser() ) ) {
			$this->displayRestrictionError();
			return;
		}
		$output = $this->getOutput();
		$this->setHeaders();

		# Get request data from, e.g.
		$param = $request->getText( 'param' );

		# Do stuff
		# ...
		$wikitext = 'Hello world!';
		$output->addWikiText( $wikitext );
	}
}
